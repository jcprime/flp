## What's all this crap then?

Basically, ignore everything aside from `flp.tex`, the `tex` directory itself, and `flp.pdf`.

1. `flp.tex` is the main file, which gets compiled into `flp.pdf`
2. The `tex` directory contains all the individual chapters, taken from the html files (thanks go to Caltech for making them freely available via browser, but I find their online layout to be rather difficult to read, so I'm trying to compile them to my own preferences offline)
