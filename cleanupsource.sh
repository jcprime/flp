#!/bin/bash
# cleanupsource.sh
# Keeping track of all the changes I've made to clean up FLP source files using Perl regex search-and-replace functionality

regex_file="$HOME/flp/replacements.csv"

# Ignore "comment" lines beginning with "#" AND blank lines!
while read -r line; do
    sed '' "$line"
done
