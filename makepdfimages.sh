#!/bin/bash
# makepdfimages.sh
# Generates all the .pdf files from .svg files in img/ directory for FLP

img_path="$HOME/flp/img"
cd "$img_path" || exit

function gunzip_svgz() {
	# This outputs files without extensions, i.e. that just end in "."
	gunzip -f -S svgz *.svgz
}

function extensionless_to_svg() {
	for file in *.; do
		mv "$file" "${file}svg"
	done
}

# Obsolete - superceded by svg_to_pdf_via_inkscape!
# function svg_to_pdf() {
# 	file="$1"
# 	ext="${file##.}"
# 	if [[ "$ext" == "svg" ]]; then
# 	    svg2pdf "$file" "${file%.svg}.pdf" || return
# 	else
# 		return
# 	fi
# }

function svg_to_pdf_via_inkscape() {
	# Needs to be given ABSOLUTE paths please, hence the full inclusion here
	if [[ "$#" -gt "0" ]]; then
		path="$1"
	elif [[ "$#" -eq "0" ]]; then
		# Not sure if this'll work without the whole thing being quoted, but here's hoping!
		path="$HOME/flp/img/"FLP_I*/[ft]* || path="$(pwd)"
		echo "$path" # TEST
	fi
	for file in "$path/"*.svg; do
		# echo "$file" # TEST
		inkscape --export-pdf="${file%.svg}.pdf" --file="$file" || return
	done
}

# Included for posterity, but doesn't work (makes a bunch of blank PDFs)!
# function ghostscript_pdf_fail() {
# 	for pdf in *.pdf; do
# 		/usr/local/bin/gs -o "$pdf" -sDEVICE=pdfwrite -dColorConversionStrategy=/sRGB -dProcessColorModel=/DeviceRGB "$pdf"
# 	done
# }

function pdf_ps_pdf() {
	for pdf in *.pdf; do
		pdf2ps "$pdf" "${pdf%.*}.ps"
		ps2pdf "${pdf%.*}.ps" "$pdf"
	done
}

# This one's to catch the few .svg files in the main img/ directory...
gunzip_svgz
extensionless_to_svg
# for file in *; do
svg_to_pdf_via_inkscape "$(pwd)"
# done
pdf_ps_pdf

# And now for the individual volume directories in turn...
for dir in FLP_*; do
	for dir2 in [ft]*; do
		cd "$dir"
		gunzip_svgz
		extensionless_to_svg
		# for file in *.svg; do
			# svg_to_pdf "$file"
			svg_to_pdf_via_inkscape "$(pwd)"
		# done
		pdf_ps_pdf
		cd ..
	done
	cd ..
done
