\hypertarget{Ch13}{}
\chapter{\texorpdfstring{{13}Work and Potential Energy (A)}{13Work and Potential Energy (A)}}\label{work-and-potential-energy-a}

\hypertarget{Ch13-SUM}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-00/f13-00a.jpg}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-00/f13-00b.jpg}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-00/f13-00c.jpg}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-00/f13-00d.jpg}

\hypertarget{Ch13-S1}{}
\subsubsection{\texorpdfstring{{13--1}Energy of a falling body}{13--1Energy of a falling body}}\label{energy-of-a-falling-body}

\hypertarget{Ch13-S1-p1}{}
In Chapter~\href{I_04.html}{4} we discussed the conservation of energy. In that discussion, we did not use Newton's laws, but it is, of course, of great interest to see how it comes about that energy is in fact conserved in accordance with these laws. For clarity we shall start with the simplest possible example, and then develop harder and harder examples.

\hypertarget{Ch13-S1-p2}{}
The simplest example of the conservation of energy is a vertically falling object, one that moves only in a vertical direction. An object which changes its height under the influence of gravity alone has a kinetic energy~$T$ (or K.E.) due to its motion during the fall, and a potential energy~$mgh$, abbreviated~$U$ (or P.E.), whose sum is constant: \begin{equation}
\underset{\text{K.E.}}{\tfrac{1}{2}mv^2}+
\underset{\text{P.E.}}{\vphantom{\tfrac{1}{2}}mgh}=\text{const},\notag \end{equation} or \begin{equation}
\label{Eq:I:13:1} T+U=\text{const}.
\end{equation} Now we would like to show that this statement is true. What do we mean, show it is true? From Newton's Second Law we can easily tell how the object moves, and it is easy to find out how the velocity varies with time, namely, that it increases proportionally with the time, and that the height varies as the square of the time. So if we measure the height from a zero point where the object is stationary, it is no miracle that the height turns out to be equal to the square of the velocity times a number of constants. However, let us look at it a little more closely.

\hypertarget{Ch13-S1-p3}{}
Let us find out \emph{directly} from Newton's Second Law how the kinetic energy should change, by taking the derivative of the kinetic energy with respect to time and then using Newton's laws. When we differentiate~$\tfrac{1}{2}mv^2$ with respect to time, we obtain \begin{equation}
\label{Eq:I:13:2}
\frac{dT}{dt}=\frac{d}{dt}\,(\tfrac{1}{2}mv^2)=
\tfrac{1}{2}m2v\,\frac{dv}{dt}=mv\,\frac{dv}{dt}, \end{equation} since $m$ is assumed constant. But from Newton's Second Law, $m(dv/dt)=F$, so that \begin{equation} \label{Eq:I:13:3}
dT/dt = Fv. \end{equation} In general, it will come out to be~$\vec F\cdot\vec v$, but in our one-dimensional case let us leave it as the force times the velocity.

\hypertarget{Ch13-S1-p4}{}
Now in our simple example the force is constant, equal to~$-mg$, a vertical force (the minus sign means that it acts downward), and the velocity, of course, is the rate of change of the vertical position, or height~$h$, with time. Thus the rate of change of the kinetic energy is~$-mg(dh/dt)$, which quantity, miracle of miracles, is minus the rate of change of something else! It is minus the time rate of change of~$mgh$! Therefore, as time goes on, the changes in kinetic energy and in the quantity~$mgh$ are equal and opposite, so that the sum of the two quantities remains constant. Q.E.D.

\hypertarget{Ch13-F1}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-01/f13-01_tc_big.pdf}
 {Fig. 13--1.}An object moving on a frictionless curve under the influence of gravity.

\hypertarget{Ch13-S1-p5}{}
We have shown, from Newton's second law of motion, that energy is conserved for constant forces when we add the potential energy~$mgh$ to the kinetic energy~$\tfrac{1}{2}mv^2$. Now let us look into this further and see whether it can be generalized, and thus advance our understanding. Does it work only for a freely falling body, or is it more general? We expect from our discussion of the conservation of energy that it would work for an object moving from one point to another in some kind of frictionless curve, under the influence of gravity (Fig.~\href{I_13.html\#Ch13-F1}{13--1}). If the object reaches a certain height~$h$ from the original height~$H$, then the same formula should again be right, even though the velocity is now in some direction other than the vertical. We would like to understand \emph{why} the law is still correct. Let us follow the same analysis, finding the time rate of change of the kinetic energy. This will again be~$mv(dv/dt)$, but $m(dv/dt)$ is the rate of change of the magnitude of the momentum, i.e.~the \emph{force in the direction of motion}---the tangential force~$F_t$. Thus \begin{equation*}
\frac{dT}{dt}=mv\,\frac{dv}{dt}=F_tv.
\end{equation*} Now the speed is the rate of change of distance along the curve, $ds/dt$, and the tangential force~$F_t$ is not~$-mg$ but is weaker by the ratio of the vertical distance~$dh$ to the distance~$ds$ along the path. In other words, \begin{equation*}
F_t=-mg\sin\theta=-mg\,\frac{dh}{ds}, \end{equation*} so that \begin{equation*}
F_t\,\frac{ds}{dt}=-mg\biggl(\frac{dh}{ds}\biggr)
\biggl(\frac{ds}{dt}\biggr)=-mg\,\frac{dh}{dt}, \end{equation*} since the $ds$'s cancel. Thus we get $-mg(dh/dt)$, which is equal to the rate of change of~$-mgh$, as before.

\hypertarget{Ch13-S1-p6}{}
In order to understand exactly how the conservation of energy works in general in mechanics, we shall now discuss a number of concepts which will help us to analyze it.

\hypertarget{Ch13-S1-p7}{}
First, we discuss the rate of change of kinetic energy in general in three dimensions. The kinetic energy in three dimensions is \begin{equation*}
T=\tfrac{1}{2}m(v_x^2+v_y^2+v_z^2).
\end{equation*} When we differentiate this with respect to time, we get three terrifying terms:
\begin{equation} \label{Eq:I:13:4}
\frac{dT}{dt}=m\biggl( v_x\,\frac{dv_x}{dt}+ v_y\,\frac{dv_y}{dt}+ v_z\,\frac{dv_z}{dt}
\biggr). \end{equation} But $m(dv_x/dt)$ is the force~$F_x$ acting on the object in the $x$-direction. Thus the right side of Eq.~(\href{I_13.html\#mjx-eqn-EqI134}{13.4}) is~$F_x v_x + F_y v_y + F_z v_z$. We recall our vector analysis and recognize this as~$\vec F\cdot\vec v$; therefore \begin{equation}
\label{Eq:I:13:5}
dT/dt=\vec F\cdot\vec v.
\end{equation} This result can be derived more quickly as follows: if $\vec a$ and~$\vec b$ are two vectors, both of which may depend upon the time, the derivative of~$\vec a\cdot\vec b$ is, in general, \begin{equation}
\label{Eq:I:13:6}
d(\vec a\cdot\vec b)/dt=\vec a\cdot(d\vec b/dt)+(d\vec a/dt)\cdot\vec b.
\end{equation} We then use this in the form $\vec a =$ $\vec b =$ $\vec v$:
\begin{equation}
\label{Eq:I:13:7}
\frac{d(\tfrac{1}{2}mv^2)}{dt}=\frac{d(\tfrac{1}{2}m\vec v\cdot\vec v)}{dt}= m\,\frac{d\vec v}{dt}\cdot\vec v=\vec F\cdot\vec v=\vec F\cdot\frac{d\vec s}{dt}.
\end{equation}

\hypertarget{Ch13-S1-p8}{}
Because the concepts of kinetic energy, and energy in general, are so important, various names have been given to the important terms in equations such as these. $\tfrac{1}{2}mv^2$ is, as we know, called \emph{kinetic energy}. $\vec F\cdot\vec v$ is called \emph{power}: the force acting on an object times the velocity of the object (vector ``dot'' product) is the power being delivered to the object by that force. We thus have a marvelous theorem: \emph{the rate of change of kinetic energy of an object is equal to the power expended by the forces acting on it}.

\hypertarget{Ch13-S1-p9}{}
However, to study the conservation of energy, we want to analyze this still more closely. Let us evaluate the change in kinetic energy in a very short time~$dt$. If we multiply both sides of Eq.~(\href{I_13.html\#mjx-eqn-EqI137}{13.7}) by~$dt$, we find that the differential change in the kinetic energy is the force ``dot'' the differential distance moved: \begin{equation}
\label{Eq:I:13:8}
dT=\vec F\cdot d\vec s.
\end{equation} If we now integrate, we get \begin{equation} \label{Eq:I:13:9}
\Delta T=\int_1^2\vec F\cdot d\vec s. \end{equation} What does this mean? It means that if an object is moving \emph{in any way} under the influence of a force, moving in some kind of curved path, then the change in K.E. when it goes from one point to another along the curve is equal to the integral of the component of the force along the curve times the differential displacement~$ds$, the integral being carried out from one point to the other. This integral also has a name; it is called the \emph{work done by the force on the object}. We see immediately that \emph{power equals work done per second}. We also see that it is only a component of force \emph{in the direction of motion}
that contributes to the work done. In our simple example the forces were only vertical, and had only a single component, say~$F_z$, equal to~$-mg$. No matter how the object moves in those circumstances, falling in a parabola for example, $\vec F\cdot\vec s$, which can be written as~$F_x\,dx + F_y\,dy + F_z\,dz$, has nothing left of it but~$F_z\,dz = -mg\,dz$, because the other components of force are zero. Therefore, in our simple case, \begin{equation} \label{Eq:I:13:10}
\int_1^2\vec F\cdot d\vec s=\int_{z_1}^{z_2}-mg\,dz=-mg(z_2-z_1), \end{equation} so again we find that it is only the \emph{vertical height} from which the object falls that counts toward the potential energy.

\hypertarget{Ch13-S1-p10}{}
A word about units. Since forces are measured in newtons, and we multiply by a distance in order to obtain work, work is measured in \emph{newton}${}\cdot{}$\emph{meters}
(N${}\cdot{}$m), but people do not like to say newton-meters, they prefer to say \emph{joules}~(J). A newton-meter is called a joule; work is measured in joules. Power, then, is joules per second, and that is also called a \emph{watt}~(W). If we multiply watts by time, the result is the work done. The work done by the electrical company in our houses, technically, is equal to the watts times the time. That is where we get things like kilowatt hours, $1000$~watts times $3600$~seconds, or $3.6\times10^6$~joules.

\hypertarget{Ch13-S1-p11}{}
Now we take another example of the law of conservation of energy. Consider an object which initially has kinetic energy and is moving very fast, and which slides against the floor with friction. It stops. At the start the kinetic energy is \emph{not} zero, but at the end it \emph{is}
zero; there is work done by the forces, because whenever there is friction there is always a component of force in a direction opposite to that of the motion, and so energy is steadily lost. But now let us take a mass on the end of a pivot swinging in a vertical plane in a gravitational field with no friction. What happens here is different, because when the mass is going up the force is downward, and when it is coming down, the force is also downward. Thus $\vec F\cdot d\vec s$ has one sign going up and another sign coming down. At each corresponding point of the downward and upward paths the values of~$\vec F\cdot d\vec s$ are exactly equal in size but of opposite sign, so the net result of the integral will be zero for this case. Thus the kinetic energy with which the mass comes back to the bottom is the same as it had when it left; that is the principle of the conservation of energy. (Note that when there are friction forces the conservation of energy seems at first sight to be invalid. We have to find another \emph{form} of energy. It turns out, in fact, that \emph{heat} is generated in an object when it rubs another with friction, but at the moment we supposedly do not know that.)

\hypertarget{Ch13-S2}{}
\subsubsection{\texorpdfstring{{13--2}Work done by gravity}{13--2Work done by gravity}}\label{work-done-by-gravity}

\hypertarget{Ch13-S2-p1}{}
The next problem to be discussed is much more difficult than the above; it has to do with the case when the forces are not constant, or simply vertical, as they were in the cases we have worked out. We want to consider a planet, for example, moving around the sun, or a satellite in the space around the earth.

\hypertarget{Ch13-S2-p2}{}
We shall first consider the motion of an object which starts at some point~$1$ and falls, say, \emph{directly} toward the sun or toward the earth (Fig.~\href{I_13.html\#Ch13-F2}{13--2}). Will there be a law of conservation of energy in these circumstances? The only difference is that in this case, the force is \emph{changing} as we go along, it is not just a constant. As we know, the force is~$-GM/r^2$ times the mass~$m$, where $m$ is the mass that moves. Now certainly when a body falls toward the earth, the kinetic energy increases as the distance fallen increases, just as it does when we do not worry about the variation of force with height. The question is whether it is possible to find another formula for potential energy different from~$mgh$, a different function of distance away from the earth, so that conservation of energy will still be true.

\hypertarget{Ch13-F2}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-02/f13-02_tc_big.pdf}
 {Fig. 13--2.}A small mass~$m$ falls under the influence of gravity toward a large mass~$M$.

\hypertarget{Ch13-S2-p3}{}
This one-dimensional case is easy to treat because we know that the change in the kinetic energy is equal to the integral, from one end of the motion to the other, of~$-GMm/r^2$ times the displacement~$dr$: \begin{equation}
\label{Eq:I:13:11}
T_2-T_1=-\int_1^2GMm\,\frac{dr}{r^2}.
\end{equation} There are no cosines needed for this case because the force and the displacement are in the same direction. It is easy to integrate~$dr/r^2$; the result is~$-1/r$, so Eq.~(\href{I_13.html\#mjx-eqn-EqI1311}{13.11}) becomes \begin{equation} \label{Eq:I:13:12}
T_2-T_1=+GMm\biggr(\frac{1}{r_2}-\frac{1}{r_1}\biggr).
\end{equation} Thus we have a different formula for potential energy. Equation~(\href{I_13.html\#mjx-eqn-EqI1312}{13.12}) tells us that the quantity~$(\tfrac{1}{2}mv^2 - GMm/r)$ calculated at point~$1$, at point~$2$, or at any other place, has a constant value.

\hypertarget{Ch13-S2-p4}{}
We now have the formula for the potential energy in a gravitational field for vertical motion. Now we have an interesting problem. Can we make \emph{perpetual motion} in a gravitational field? The gravitational field varies; in different places it is in different directions and has different strengths. Could we do something like this, using a fixed, frictionless track: start at some point and lift an object out to some other point, then move it around an arc to a third point, then lower it a certain distance, then move it in at a certain slope and pull it out some other way, so that when we bring it back to the starting point, a certain amount of work has been done by the gravitational force, and the kinetic energy of the object is increased? Can we design the curve so that it comes back moving a little bit faster than it did before, so that it goes around and around and around, and gives us perpetual motion? Since perpetual motion is impossible, we ought to find out that this is also impossible. We ought to discover the following proposition: since there is no friction the object should come back with neither higher nor lower velocity---it should be able to keep going around and around any closed path. Stated in another way, \emph{the total work done in going around a complete cycle should be zero} for gravity forces, because if it is not zero we can get energy out by going around. (If the work turns out to be less than zero, so that we get less speed when we go around one way, then we merely go around the other way, because the forces, of course, depend only upon the position, not upon the direction; if one way is plus, the other way would be minus, so unless it is zero we will get perpetual motion by going around either way.)

\hypertarget{Ch13-F3}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-03/f13-03_tc_big.pdf}
 {Fig. 13--3.}A closed path in a gravitational field.

\hypertarget{Ch13-S2-p5}{}
Is the work really zero? Let us try to demonstrate that it is. First we shall explain more or less why it is zero, and then we shall examine it a little better mathematically. Suppose that we use a simple path such as that shown in Fig.~\href{I_13.html\#Ch13-F3}{13--3}, in which a small mass is carried from point~$1$ to point~$2$, and then is made to go around a circle to~$3$, back to~$4$, then to $5$,~$6$, $7$, and~$8$, and finally back to~$1$. All of the lines are either purely radial or circular, with~$M$ as the center. How much work is done in carrying $m$ around this path? Between points $1$ and~$2$, it is $GMm$ times the difference of~$1/r$ between these two points:
\begin{equation*}
W_{12}=\int_1^2\vec F\cdot d\vec s=\int_1^2-GMm\,\frac{dr}{r^2}= GMm\biggr(\frac{1}{r_2}-\frac{1}{r_1}\biggr).
\end{equation*} From $2$ to~$3$ the force is exactly at right angles to the curve, so that $W_{23}\equiv0$. The work from $3$ to~$4$ is \begin{equation*}
W_{34}=\int_3^4\vec F\cdot d\vec s= GMm\biggr(\frac{1}{r_4}-\frac{1}{r_3}\biggr).
\end{equation*} In the same fashion, we find that $W_{45} = 0$, $W_{56} =GMm(1/r_6 - 1/r_5)$,~$W_{67} = 0$, $W_{78} =GMm(1/r_8 - 1/r_7)$, and~$W_{81} = 0$. Thus \begin{equation*} W=GMm\biggl(
\frac{1}{r_2}-\frac{1}{r_1}+
\frac{1}{r_4}-\frac{1}{r_3}+
\frac{1}{r_6}-\frac{1}{r_5}+
\frac{1}{r_8}-\frac{1}{r_7}
\biggr). \end{equation*} But we note that $r_2 = r_3$,~$r_4 = r_5$, $r_6 = r_7$, and~$r_8 = r_1$. Therefore~$W=0$.

\hypertarget{Ch13-F4}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-04/f13-04_tc_big.pdf}
 {Fig. 13--4.}A ``smooth'' closed path, showing a magnified segment of it approximated by a series of radial and circumferential steps, and an enlarged view of one step.

\hypertarget{Ch13-S2-p6}{}
Of course we may wonder whether this is too trivial a curve. What if we use a \emph{real} curve? Let us try it on a real curve. First of all, we might like to assert that a real curve could always be imitated sufficiently well by a series of sawtooth jiggles like those of Fig.~\href{I_13.html\#Ch13-F4}{13--4}, and that therefore, etc., Q.E.D., but without a little analysis, it is not obvious at first that the work done going around even a small triangle is zero. Let us magnify one of the triangles, as shown in Fig.~\href{I_13.html\#Ch13-F4}{13--4}. Is the work done in going from $a$ to~$b$ and $b$ to~$c$ on a triangle the same as the work done in going directly from $a$ to~$c$? Suppose that the force is acting in a certain direction; let us take the triangle such that the side~$bc$ is in this direction, just as an example. We also suppose that the triangle is so small that the force is essentially constant over the entire triangle. What is the work done in going from $a$ to~$c$? It is \begin{equation*}
W_{ac}=\int_a^c\vec F\cdot d\vec s=Fs\cos\theta, \end{equation*} since the force is constant. Now let us calculate the work done in going around the other two sides of the triangle. On the vertical side~$ab$ the force is perpendicular to~$d\vec s$, so that here the work is zero. On the horizontal side~$bc$, \begin{equation*}
W_{bc}=\int_b^c\vec F\cdot d\vec s=Fx. \end{equation*} Thus we see that the work done in going along the sides of a small triangle is the same as that done going on a slant, because $s\cos\theta$ is equal to~$x$. We have proved previously that the answer is zero for any path composed of a series of notches like those of Fig.~\href{I_13.html\#Ch13-F3}{13--3}, and also that we do the same work if we cut across the corners instead of going along the notches (so long as the notches are fine enough, and we can always make them very fine); therefore, \emph{the work done in going around any path in a gravitational field is zero}.

\hypertarget{Ch13-S2-p7}{}
This is a very remarkable result. It tells us something we did not previously know about planetary motion. It tells us that when a planet moves around the sun (without any other objects around, no other forces) it moves in such a manner that the square of the speed at any point minus some constants divided by the radius at that point is always the same at every point on the orbit. For example, the closer the planet is to the sun, the faster it is going, but by how much? By the following amount: if instead of letting the planet go around the sun, we were to change the direction (but not the magnitude) of its velocity and make it move radially, and then we let it fall from some special radius to the radius of interest, the new speed would be the same as the speed it had in the actual orbit, because this is just another example of a complicated path. So long as we come back to the same distance, the kinetic energy will be the same. So, whether the motion is the real, undisturbed one, or is changed in direction by channels, by frictionless constraints, the kinetic energy with which the planet arrives at a point will be the same.

\hypertarget{Ch13-S2-p8}{}
Thus, when we make a numerical analysis of the motion of the planet in its orbit, as we did earlier, we can check whether or not we are making appreciable errors by calculating this constant quantity, the energy, at every step, and it should not change. For the orbit of Table~\href{I_09.html\#Ch9-T2}{9--2} the energy does change,\href{I_13.html\#footnote_1}{\textsuperscript{1}} it changes by some $1.5$~percent from the beginning to the end. Why? Either because for the numerical method we use finite intervals, or else because we made a slight mistake somewhere in arithmetic.

\hypertarget{Ch13-S2-p9}{}
Let us consider the energy in another case: the problem of a mass on a spring. When we displace the mass from its balanced position, the restoring force is proportional to the displacement. In those circumstances, can we work out a law for conservation of energy? Yes, because the work done by such a force is \begin{equation} \label{Eq:I:13:13}
W=\int_0^xF\,dx=\int_0^x-kx\,dx=-\tfrac{1}{2}kx^2.
\end{equation} Therefore, for a mass on a spring we have that the kinetic energy of the oscillating mass plus~$\tfrac{1}{2}kx^2$ is a constant. Let us see how this works. We pull the mass down; it is standing still and so its speed is zero. But $x$ is not zero, $x$ is at its maximum, so there is some energy, the potential energy, of course. Now we release the mass and things begin to happen (the details not to be discussed), but at any instant the kinetic plus potential energy must be a constant. For example, after the mass is on its way past the original equilibrium point, the position~$x$ equals zero, but that is when it has its biggest~$v^2$, and as it gets more $x^2$ it gets less~$v^2$, and so on. So the balance of $x^2$ and~$v^2$ is maintained as the mass goes up and down. Thus we have another rule now, that the potential energy for a spring is~$\tfrac{1}{2}kx^2$, if the force is~$-kx$.

\hypertarget{Ch13-S3}{}
\subsubsection{\texorpdfstring{{13--3}Summation of energy}{13--3Summation of energy}}\label{summation-of-energy}

\hypertarget{Ch13-S3-p1}{}
Now we go on to the more general consideration of what happens when there are large numbers of objects. Suppose we have the complicated problem of many objects, which we label $i = 1$,~$2$, $3$,~\ldots{}, all exerting gravitational pulls on each other. What happens then? We shall prove that if we add the kinetic energies of all the particles, and add to this the sum, over all \emph{pairs} of particles, of their mutual gravitational potential energy, $-GMm/r_{ij}$, the total is a constant:
\begin{equation} \label{Eq:I:13:14}
\sum_i\tfrac{1}{2}m_iv_i^2\;+\!\!\sum_{(\text{pairs $ij$})}
\!\!-\frac{Gm_im_j}{r_{ij}}=\text{const}.
\end{equation} How do we prove it? We differentiate each side with respect to time and get zero. When we differentiate~$\tfrac{1}{2}m_iv_i^2$, we find derivatives of the velocity that are the forces, just as in Eq.~(\href{I_13.html\#mjx-eqn-EqI135}{13.5}). We replace these forces by the law of force that we know from Newton's law of gravity and then we notice that what is left is minus the time derivative of \begin{equation*}
\sum_{\text{pairs}}
-\frac{Gm_im_j}{r_{ij}}.
\end{equation*} The time derivative of the kinetic energy is \begin{equation}
\begin{aligned}
\frac{d}{dt}\sum_i\tfrac{1}{2}m_iv_i^2  & =\sum_im_i\,\frac{d\vec v_i}{dt}\cdot\vec v_i  & =\sum_i\vec F_i\cdot\vec v_i{[}-.5ex{]}
 & =\sum_i\Biggl(
\sum_j-\frac{Gm_im_j\vec r_{ij}}{r_{ij}^3}
\Biggr)\cdot\vec v_i.
\end{aligned} \label{Eq:I:13:15}
\end{equation} The time derivative of the potential energy is \begin{equation*}
\frac{d}{dt}\sum_{\text{pairs}}
-\frac{Gm_im_j}{r_{ij}}=
\sum_{\text{pairs}}
\Biggl(+\frac{Gm_im_j}{r_{ij}^2}\Biggr)
\biggl(\frac{dr_{ij}}{dt}\biggr).
\end{equation*} But \begin{equation*}
r_{ij}=\sqrt{(x_i-x_j)^2+(y_i-y_j)^2+(z_i-z_j)^2}, \end{equation*} so that \begin{align*}
\frac{dr_{ij}}{dt} & =
\begin{alignedat}{8}
\frac{1}{2r_{ij}}\biggl{[}
 & 2(x_i & -x_j &)\biggl(\frac{dx_i}{dt} & -\frac{dx_j}{dt} & \biggr)\\ {}+{} & 2(y_i & -y_j & )\biggl(\frac{dy_i}{dt} & -\frac{dy_j}{dt} & \biggr)\\ {}+{} & 2(z_i & -z_j & )\biggl(\frac{dz_i}{dt} & -\frac{dz_j}{dt} & \biggr)
\biggr{]}
\end{alignedat} \\  & =\vec r_{ij}\cdot\frac{\vec v_i-\vec v_j}{r_{ij}} \\  & =\vec r_{ij}\cdot\frac{\vec v_i}{r_{ij}}+
\vec r_{ji}\cdot\frac{\vec v_j}{r_{ji}}, \end{align*} since $\vec r_{ij} = -\vec r_{ji}$, while $r_{ij} = r_{ji}$. Thus \begin{equation} \label{Eq:I:13:16}
\frac{d}{dt}\sum_{\text{pairs}}-\frac{Gm_im_j}{r_{ij}}=
\sum_{\text{pairs}}\biggl{[}
\frac{Gm_im_j\vec r_{ij}}{r_{ij}^3}\cdot\vec v_i+
\frac{Gm_jm_i\vec r_{ji}}{r_{ji}^3}\cdot\vec v_j\biggr{]}.
\end{equation}
Now we must note carefully what $\sum\limits_i{\sum\limits_j}$ and~$\sum\limits_{\text{pairs}}$ mean. In Eq.~(\href{I_13.html\#mjx-eqn-EqI1315}{13.15}), $\sum\limits_i{\sum\limits_j}$ means that $i$ takes on all values $i=1$,~$2$, $3$,~\ldots{}~in turn, and for each value of~$i$, the index~$j$ takes on all values except~$i$. Thus if~$i = 3$, $j$ takes on the values $1$,~$2$, $4$,~\ldots{}

\hypertarget{Ch13-S3-p2}{}
In Eq.~(\href{I_13.html\#mjx-eqn-EqI1316}{13.16}), on the other hand, $\sum\limits_{\text{pairs}}$ means that given values of $i$ and~$j$ occur only once. Thus the particle pair $1$ and~$3$ contributes only one term to the sum. To keep track of this, we might agree to let $i$ range over all values $1$,~$2$, $3$,~\ldots{}, and for each $i$ let $j$ range only over values \emph{greater} than $i$. Thus if $i = 3$, $j$ could only have values $4$,~$5$, $6$,~\ldots{}~But we notice that for each $i,j$~value there are two contributions to the sum, one involving $\vec v_i$, and the other $\vec v_j$, and that these terms have the same appearance as those of Eq.~(\href{I_13.html\#mjx-eqn-EqI1315}{13.15}), where \emph{all} values of $i$ and~$j$ (except $i = j$) are included in the sum. Therefore, by matching the terms one by one, we see that Eqs. (\href{I_13.html\#mjx-eqn-EqI1316}{13.16}) and~(\href{I_13.html\#mjx-eqn-EqI1315}{13.15}) are precisely the same, but of opposite sign, so that the time derivative of the kinetic plus potential energy is indeed zero. Thus we see that, for many objects, \emph{the kinetic energy is the sum of the contributions from each individual object}, and that the potential energy is also simple, it being also just a sum of contributions, the energies between all the pairs. We can understand \emph{why} it should be the energy of every pair this way: Suppose that we want to find the total amount of work that must be done to bring the objects to certain distances from each other. We may do this in several steps, bringing them in from infinity where there is no force, one by one. First we bring in number one, which requires no work, since no other objects are yet present to exert force on it. Next we bring in number two, which does take some work, namely~$W_{12}=-Gm_1m_2/r_{12}$. Now, and this is an important point, suppose we bring in the next object to position three. At any moment the force on number~$3$ can be written as the sum of two forces---the force exerted by number~$1$ and that exerted by number~$2$. Therefore \emph{the work done is the sum of the works done by each}, because if~$\vec F_3$ can be resolved into the sum of two forces, \begin{equation*}
\vec F_3=\vec F_{13}+\vec F_{23}, \end{equation*} then the work is \begin{equation*}
\int\vec F_3\cdot d\vec s=
\int\vec F_{13}\cdot d\vec s+\int\vec F_{23}\cdot d\vec s= W_{13}+W_{23}.
\end{equation*} That is, the work done is the sum of the work done against the first force and the second force, as if each acted independently. Proceeding in this way, we see that the total work required to assemble the given configuration of objects is precisely the value given in Eq.~(\href{I_13.html\#mjx-eqn-EqI1314}{13.14}) as the potential energy. It is because gravity obeys the principle of superposition of forces that we can write the potential energy as a sum over each pair of particles.

\hypertarget{Ch13-S4}{}
\subsubsection{\texorpdfstring{{13--4}Gravitational field of large objects}{13--4Gravitational field of large objects}}\label{gravitational-field-of-large-objects}

\hypertarget{Ch13-F5}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-05/f13-05_tc_big.pdf}
 {Fig. 13--5.}The gravitational field~$\vec C$ at a mass point produced by an infinite plane sheet of matter.

\hypertarget{Ch13-S4-p1}{}
Now we shall calculate the fields which are met in a few physical circumstances involving \emph{distributions of mass}. We have not so far considered distributions of mass, only particles, so it is interesting to calculate the forces when they are produced by more than just one particle. First we shall find the gravitational force on a mass that is produced by a plane sheet of material, infinite in extent. The force on a unit mass at a given point~$P$, produced by this sheet of material (Fig.~\href{I_13.html\#Ch13-F5}{13--5}), will of course be directed toward the sheet. Let the distance of the point from the sheet be~$a$, and let the amount of mass per unit area of this huge sheet be~$\mu$. We shall suppose $\mu$ to be constant; it is a uniform sheet of material. Now, what small field~$d\vec C$ is produced by the mass~$dm$ lying between $\rho$ and~$\rho + d\rho$ from the point~$O$ of the sheet nearest point~$P$? Answer:~$d\vec C = -G(dm\,\vec r/r^3)$. But this field is directed along~$\vec r$, and we know that only the $x$-component of it will remain when we add all the little vector~$d\vec C$'s to produce~$\vec C$. The $x$-component of~$d\vec C$ is \begin{equation*}
dC_x=-G\,\frac{dm\,r_x}{r^3}=-G\,\frac{dm\,a}{r^3}.
\end{equation*} Now all masses~$dm$ which are at the same distance~$r$ from~$P$ will yield the same~$dC_x$, so we may at once write for~$dm$ the total mass in the \emph{ring} between $\rho$ and~$\rho+d\rho$, namely $dm =
\mu2\pi\rho\,d\rho$ ($2\pi\rho\,d\rho$ is the area of a ring of radius~$\rho$ and width~$d\rho$, if $d\rho\ll\rho$). Thus \begin{equation*}
dC_x=-G\mu2\pi\rho\,\frac{d\rho\,a}{r^3}.
\end{equation*}
Then, since $r^2 =
\rho^2 + a^2$, $\rho\,d\rho = r\,dr$. Therefore, \begin{equation}
\label{Eq:I:13:17} C_x=-2\pi G\mu a\int_a^\infty\frac{dr}{r^2}= -2\pi G\mu a\Bigl(\frac{1}{a}-\frac{1}{\infty}\Bigr)=-2\pi G\mu.
\end{equation}
Thus the force is independent of distance~$a$! Why? Have we made a mistake? One might think that the farther away we go, the weaker the force would be. But no! If we are close, most of the matter is pulling at an unfavorable angle; if we are far away, more of the matter is situated more favorably to exert a pull toward the plane. At any distance, the matter which is most effective lies in a certain cone. When we are farther away the force is smaller by the inverse square, but in the same cone, in the same angle, there is much \emph{more matter}, larger by just the square of the distance! This analysis can be made rigorous by just noticing that the differential contribution in any given cone is in fact independent of the distance, because of the reciprocal variation of the strength of the force from a given mass, and the amount of mass included in the cone, with changing distance. The force is not really constant of course, because when we go on the other side of the sheet it is reversed in sign.

\hypertarget{Ch13-S4-p2}{}
We have also, in effect, solved an electrical problem: if we have an electrically charged plate, with an amount~$\sigma$ of charge per unit area, then the electric field at a point outside the sheet is equal to~$\sigma/2\epsilon_{0}$, and is in the outward direction if the sheet is positively charged, and inward if the sheet is negatively charged. To prove this, we merely note that~$-G$, for gravity, plays the same role as~$1/4\pi\epsilon_{0}$ for electricity.

\hypertarget{Ch13-S4-p3}{}
Now suppose that we have two plates, with a positive charge~$+\sigma$ on one and a negative charge~$-\sigma$ on another at a distance~$D$ from the first. What is the field? Outside the two plates it is zero. Why? Because one attracts and the other repels, the force being \emph{independent of distance}, so that the two balance out! Also, the field \emph{between} the two plates is clearly twice as great as that from one plate, namely~$E=\sigma/\epsilon_{0}$, and is directed from the positive plate to the negative one.

\hypertarget{Ch13-S4-p4}{}
Now we come to a most interesting and important problem, whose solution we have been assuming all the time, namely, that the force produced by the earth at a point on the surface or outside it is the same as if all the mass of the earth were located at its center. The validity of this assumption is not obvious, because when we are close, some of the mass is very close to us, and some is farther away, and so on. When we add the effects all together, it seems a miracle that the net force is exactly the same as we would get if we put all the mass in the middle!

\hypertarget{Ch13-F6}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f13-06/f13-06_tc_big.pdf}
 {Fig. 13--6.}A thin spherical shell of mass or charge.

\hypertarget{Ch13-S4-p5}{}
We now demonstrate the correctness of this miracle. In order to do so, however, we shall consider a thin uniform hollow shell instead of the whole earth. Let the total mass of the shell be~$m$, and let us calculate the \emph{potential energy} of a particle of mass~$m'$ a distance~$R$ away from the center of the sphere (Fig.~\href{I_13.html\#Ch13-F6}{13--6}) and show that the potential energy is the same as it would be if the mass~$m$ were a point at the center. (The potential energy is easier to work with than is the field because we do not have to worry about angles, we merely add the potential energies of all the pieces of mass.) If we call~$x$ the distance of a certain plane section from the center, then all the mass that is in a slice~$dx$ is at the same distance~$r$ from~$P$, and the potential energy due to this ring is~$-Gm'\,dm/r$. How much mass is in the small slice~$dx$? An amount \begin{equation*} dm=2\pi y\mu\,ds=\frac{2\pi y\mu\,dx}{\sin\theta}=
\frac{2\pi y\mu\,dx\,a}{y}=2\pi a\mu\,dx, \end{equation*}
where $\mu=m/4\pi a^2$ is the surface density of mass on the spherical shell. (It is a general rule that the area of a zone of a sphere is proportional to its axial width.) Therefore the potential energy due to~$dm$ is \begin{equation*}
dW=-\frac{Gm'\,dm}{r}=-\frac{Gm'2\pi a\mu\,dx}{r}.
\end{equation*} But we see that \begin{equation*} \begin{aligned}
r^2=y^2+(R-x)^2 & =y^2+x^2+R^2-2Rx  & =a^2+R^2-2Rx. \end{aligned}
\end{equation*} Thus \begin{equation*}
2r\,dr=-2R\,dx \end{equation*} or \begin{equation*}
\frac{dx}{r}=-\frac{dr}{R}.
\end{equation*} Therefore, \begin{equation*}
dW=\frac{Gm'2\pi a\mu\,dr}{R}, \end{equation*} and so \begin{align}
W & =\frac{Gm'2\pi a\mu}{R}\int_{R+a}^{R-a}dr\notag  & =-\frac{Gm'2\pi a\mu}{R}\,2a= -\frac{Gm'(4\pi a^2\mu)}{R}\notag \label{Eq:I:13:18}
 & =-\frac{Gm'm}{R}. \end{align}
Thus, for a thin spherical shell, the potential energy of a mass~$m'$, external to the shell, is the same as though the mass of the shell were concentrated at its center. The earth can be imagined as a series of spherical shells, each one of which contributes an energy which depends only on its mass and the distance from its center to the particle; adding them all together we get the \emph{total mass}, and therefore the earth acts as though all the material were at the center!

\hypertarget{Ch13-S4-p6}{}
But notice what happens if our point is on the \emph{inside} of the shell. Making the same calculation, but with~$P$ on the inside, we still get the difference of the two~$r$'s, but now in the form $a - R - (a + R) = -2R$, or minus twice the distance from the center. In other words, $W$ comes out to be $W=-Gm'm/a$, which is \emph{independent}
of~$R$ and independent of position, i.e.~the same energy no matter \emph{where} we are inside. Therefore no force; no work is done when we move about inside. If the potential energy is the same no matter where an object is placed inside the sphere, there can be no force on it. So there is no force inside, there is only a force outside, and the force outside is the same as though the mass were all at the center.

\begin{enumerate}
\tightlist \item   \protect\hypertarget{footnote_1}{}{} The energy per unit mass   is~$\tfrac{1}{2}(v_x^2 + v_y^2) - 1/r$ in   the units of Table~\href{9--2}.   % \href{I_13.html\#footnote_source_1}{↩}
\end{enumerate}
