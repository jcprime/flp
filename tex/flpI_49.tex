\hypertarget{Ch49}{}
\chapter{\texorpdfstring{{49}Modes}{49Modes}}\label{modes}

\hypertarget{Ch49-SUM}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f49-00/f49-00.jpg}

\hypertarget{Ch49-S1}{}
\subsubsection{\texorpdfstring{{49--1}The reflection of waves}{49--1The reflection of waves}}\label{the-reflection-of-waves}

\hypertarget{Ch49-S1-p1}{}
This chapter will consider some of the remarkable phenomena which are a result of confining waves in some finite region. We will be led first to discover a few particular facts about vibrating strings, for example, and then the generalization of these facts will give us a principle which is probably the most far-reaching principle of mathematical physics.

\hypertarget{Ch49-S1-p2}{}
Our first example of confining waves will be to confine a wave at one boundary. Let us take the simple example of a one-dimensional wave on a string. One could equally well consider sound in one dimension against a wall, or other situations of a similar nature, but the example of a string will be sufficient for our present purposes. Suppose that the string is held at one end, for example by fastening it to an ``infinitely solid'' wall. This can be expressed mathematically by saying that the displacement~$y$ of the string at the position~$x = 0$ must be zero, because the end does not move. Now if it were not for the wall, we know that the general solution for the motion is the sum of two functions, $F(x - ct)$ and~$G(x + ct)$, the first representing a wave travelling one way in the string, and the second a wave travelling the other way in the string: \begin{equation}
\label{Eq:I:49:1} y = F(x - ct) + G(x + ct)
\end{equation} is the general solution for any string. But we have next to satisfy the condition that the string does not move at one end. If we put~$x = 0$ in Eq.~(\href{I_49.html\#mjx-eqn-EqI491}{49.1}) and examine~$y$ for any value of~$t$, we get~$y = F(-ct) + G(+ct)$. Now if this is to be zero for all times, it means that the function~$G(ct)$ must be~$-F(-ct)$. In other words, $G$~of anything must be $-F$~of minus that same thing. If this result is put back into Eq.~(\href{I_49.html\#mjx-eqn-EqI491}{49.1}), we find that the solution for the problem is \begin{equation}
\label{Eq:I:49:2} y = F(x - ct) - F(-x - ct).
\end{equation} It is easy to check that we will get~$y = 0$ if we set~$x = 0$.

\hypertarget{Ch49-F1}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f49-01/f49-01_tc_big.pdf}
 {Fig. 49--1.}Reflection of a wave as a superposition of two travelling waves.

\hypertarget{Ch49-S1-p3}{}
Figure~\href{I_49.html\#Ch49-F1}{49--1} shows a wave travelling in the negative $x$-direction near~$x = 0$, and a hypothetical wave travelling in the other direction reversed in sign and on the other side of the origin. We say hypothetical because, of course, there is no string to vibrate on that side of the origin. The total motion of the string is to be regarded as the sum of these two waves in the region of positive~$x$. As they reach the origin, they will always cancel at~$x = 0$, and finally the second (reflected) wave will be the only one to exist for positive~$x$ and it will, of course, be travelling in the opposite direction. These results are equivalent to the following statement: if a wave reaches the clamped end of a string, it will be reflected with a change in sign. Such a reflection can always be understood by imagining that what is coming to the end of the string comes out upside down from behind the wall. In short, if we assume that the string is infinite and that whenever we have a wave going one way we have another one going the other way with the stated symmetry, the displacement at~$x = 0$ will always be zero and it would make no difference if we clamped the string there.

\hypertarget{Ch49-S1-p4}{}
The next point to be discussed is the reflection of a periodic wave. Suppose that the wave represented by~$F(x - ct)$ is a sine wave and has been reflected; then the reflected wave~$-F(-x - ct)$ is also a sine wave of the same frequency, but travelling in the opposite direction. This situation can be most simply described by using the complex function notation: $F(x - ct) = e^{i\omega(t - x/c)}$ and~$F(-x - ct) = e^{i\omega(t + x/c)}$. It can be seen that if these are substituted in~(\href{I_49.html\#mjx-eqn-EqI492}{49.2}) and if $x$ is set equal to~$0$, then $y = 0$ for all values of~$t$, so it satisfies the necessary condition. Because of the properties of exponentials, this can be written in a simpler form: \begin{equation}
\label{Eq:I:49:3} y = e^{i\omega t}(e^{-i\omega x/c} \!- e^{i\omega x/c}) = -2ie^{i\omega t}\sin\,(\omega x/c).
\end{equation}

\hypertarget{Ch49-S1-p5}{}
There is something interesting and new here, in that this solution tells us that if we look at any fixed~$x$, the string oscillates at frequency~$\omega$. No matter where this point is, the frequency is the same! But there are some places, in particular wherever $\sin\,(\omega x/c) = 0$, where there is no displacement at all. Furthermore, if at any time~$t$ we take a snapshot of the vibrating string, the picture will be a sine wave. However, the displacement of this sine wave will depend upon the time~$t$. From inspection of Eq.~(\href{I_49.html\#mjx-eqn-EqI493}{49.3}) we can see that the length of one cycle of the sine wave is equal to the wavelength of either of the superimposed waves: \begin{equation}
\label{Eq:I:49:4} \lambda = 2\pi c/\omega.
\end{equation} The points where there is no motion satisfy the condition~$\sin\,(\omega x/c) = 0$, which means that $(\omega x/c) = 0$,~$\pi$, $2\pi$, \ldots{}, $n\pi$,~\ldots{}~These points are called \emph{nodes}. Between any two successive nodes, every point moves up and down sinusoidally, but the pattern of motion stays fixed in space. This is the fundamental characteristic of what we call a \emph{mode}. If one can find a pattern of motion which has the property that at any point the object moves perfectly sinusoidally, and that all points move at the same frequency (though some will move more than others), then we have what is called a mode.

\hypertarget{Ch49-S2}{}
\subsubsection{\texorpdfstring{{49--2}Confined waves, with natural frequencies}{49--2Confined waves, with natural frequencies}}\label{confined-waves-with-natural-frequencies}

\hypertarget{Ch49-S2-p1}{}
The next interesting problem is to consider what happens if the string is held at both ends, say at $x = 0$ and~$x = L$. We can begin with the idea of the reflection of waves, starting with some kind of a bump moving in one direction. As time goes on, we would expect the bump to get near one end, and as time goes still further it will become a kind of little wobble, because it is combining with the reversed-image bump which is coming from the other side. Finally the original bump will disappear and the image bump will move in the other direction to repeat the process at the other end. This problem has an easy solution, but an interesting question is whether we can have a sinusoidal motion (the solution just described is \emph{periodic}, but of course it is not \emph{sinusoidally} periodic). Let us try to put a sinusoidally periodic wave on a string. If the string is tied at one end, we know it must look like our earlier solution~(\href{I_49.html\#mjx-eqn-EqI493}{49.3}). If it is tied at the other end, it has to look the same at the other end. So the only possibility for periodic sinusoidal motion is that the sine wave must neatly fit into the string length. If it does not fit into the string length, then it is not a natural frequency at which the string can continue to oscillate. In short, if the string is started with a sine wave shape that just fits in, then it will continue to keep that perfect shape of a sine wave and will oscillate harmonically at some frequency.

\hypertarget{Ch49-S2-p2}{}
Mathematically, we can write $\sin kx$ for the shape, where $k$ is equal to the factor~$(\omega/c)$ in Eqs. (\href{I_49.html\#mjx-eqn-EqI493}{49.3}) and~(\href{I_49.html\#mjx-eqn-EqI494}{49.4}), and this function will be zero at~$x = 0$. However, it must also be zero at the other end. The significance of this is that $k$ is no longer arbitrary, as was the case for the half-open string. With the string closed at both ends, the only possibility is that $\sin\,(kL) = 0$, because this is the only condition that will keep both ends fixed. Now in order for a sine to be zero, the angle must be either $0$, $\pi$, $2\pi$, or some other integral multiple of~$\pi$. The equation \begin{equation} \label{Eq:I:49:5} kL = n\pi \end{equation} will, therefore, give any one of the possible~$k$'s, depending on what integer is put in. For each of the~$k$'s there is a certain frequency~$\omega$, which, according to~(\href{I_49.html\#mjx-eqn-EqI493}{49.3}), is simply \begin{equation} \label{Eq:I:49:6}
\omega = kc = n\pi c/L.
\end{equation}

\hypertarget{Ch49-S2-p3}{}
So we have found the following: that a string has a property that it can have sinusoidal motions, \emph{but only at certain frequencies}. This is the most important characteristic of confined waves. No matter how complicated the system is, it always turns out that there are some patterns of motion which have a perfect sinusoidal time dependence, but with frequencies that are a property of the particular system and the nature of its boundaries. In the case of the string we have many different possible frequencies, each one, by definition, corresponding to a mode, because a mode is a pattern of motion which repeats itself sinusoidally. Figure~\href{I_49.html\#Ch49-F2}{49--2} shows the first three modes for a string. For the first mode the wavelength~$\lambda$ is~$2L$. This can be seen if one continues the wave out to~$x = 2L$ to obtain one complete cycle of the sine wave. The angular frequency~$\omega$ is~$2\pi c$ divided by the wavelength, in general, and in this case, since $\lambda$ is~$2L$, the frequency is~$\pi c/L$, which is in agreement with~(\href{I_49.html\#mjx-eqn-EqI496}{49.6}) with~$n = 1$. Let us call the first mode frequency~$\omega_1$. Now the next mode shows two loops with one node in the middle. For this mode the wavelength, then, is simply~$L$. The corresponding value of~$k$ is twice as great and the frequency is twice as large; it is~$2\omega_1$. For the third mode it is~$3\omega_1$, and so on. So all the different frequencies of the string are multiples, $1$,~$2$, $3$, $4$, and so on, of the lowest frequency~$\omega_1$.

\hypertarget{Ch49-F2}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f49-02/f49-02_tc_big.pdf}
 {Fig. 49--2.}The first three modes of a vibrating string.

\hypertarget{Ch49-S2-p4}{}
Returning now to the general motion of the string, it turns out that any possible motion can always be analyzed by asserting that more than one mode is operating at the same time. In fact, for general motion an infinite number of modes must be excited at the same time. To get some idea of this, let us illustrate what happens when there are two modes oscillating at the same time: Suppose that we have the first mode oscillating as shown by the sequence of pictures in Fig.~\href{I_49.html\#Ch49-F3}{49--3}, which illustrates the deflection of the string for equally spaced time intervals extending through half a cycle of the lowest frequency.

\hypertarget{Ch49-F3}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f49-03/f49-03_tc_big.pdf}
 {Fig. 49--3.}Two modes combine to give a travelling wave.

\hypertarget{Ch49-S2-p5}{}
Now, at the same time, we suppose that there is an oscillation of the second mode also. Figure~\href{I_49.html\#Ch49-F3}{49--3} also shows a sequence of pictures of this mode, which at the start is $90^{\circ}$~out of phase with the first mode. This means that at the start it has no displacement, but the two halves of the string have oppositely directed velocities. Now we recall a general principle relating to linear systems: if there are any two solutions, then their sum is also a solution. Therefore a third possible motion of the string would be a displacement obtained by adding the two solutions shown in Fig.~\href{I_49.html\#Ch49-F3}{49--3}. The result, also shown in the figure, begins to suggest the idea of a bump running back and forth between the ends of the string, although with only two modes we cannot make a very good picture of it; more modes are needed. This result is, in fact, a special case of a great principle for linear systems:

\begin{quote}
Any motion at all can be analyzed by assuming that it is the sum of the motions of all the different modes, combined with appropriate amplitudes and phases.
\end{quote}
 The importance of the principle derives from the fact that each mode is very simple---it is nothing but a sinusoidal motion in time. It is true that even the general motion of a string is not really very complicated, but there are other systems, for example the whipping of an airplane wing, in which the motion is much more complicated. Nevertheless, even with an airplane wing, we find there is a certain particular way of twisting which has one frequency and other ways of twisting that have other frequencies. If these modes can be found, then the complete motion can always be analyzed as a superposition of harmonic oscillations (except when the whipping is of such degree that the system can no longer be considered as linear).

\hypertarget{Ch49-S3}{}
\subsubsection{\texorpdfstring{{49--3}Modes in two dimensions}{49--3Modes in two dimensions}}\label{modes-in-two-dimensions}

\hypertarget{Ch49-S3-p1}{}
The next example to be considered is the interesting situation of modes in two dimensions. Up to this point we have talked only about one-dimensional situations---a stretched string or sound waves in a tube. Ultimately we should consider three dimensions, but an easier step will be that to two dimensions. Consider for definiteness a rectangular rubber drumhead which is confined so as to have no displacement anywhere on the rectangular edge, and let the dimensions of the rectangle be $a$ and~$b$, as shown in Fig.~\href{I_49.html\#Ch49-F4}{49--4}. Now the question is, what are the characteristics of the possible motion? We can start with the same procedure used for the string. If we had no confinement at all, we would expect waves travelling along with some kind of wave motion. For example, $(e^{i\omega t})(e^{-ik_xx + ik_yy})$ would represent a sine wave travelling in some direction which depends on the relative values of $k_x$ and $k_y$. Now how can we make the $x$-axis, that is, the line~$y = 0$, a node? Using the ideas developed for the one-dimensional string, we can imagine another wave represented by the complex function~$(-e^{i\omega t})(e^{-ik_xx - ik_yy})$. The superposition of these waves will give zero displacement at~$y = 0$ regardless of the values of $x$ and~$t$. (Although these functions are defined for negative~$y$ where there is no drumhead to vibrate, this can be ignored, since the displacement is truly zero at~$y = 0$.) In this case we can look upon the second function as the reflected wave.

\hypertarget{Ch49-F4}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f49-04/f49-04_tc_big.pdf}
 {Fig. 49--4.}Vibrating rectangular plate.

\hypertarget{Ch49-S3-p2}{}
However, we want a nodal line at~$y = b$ as well as at~$y = 0$. How do we do that? The solution is related to something we did when studying reflection from crystals. These waves which cancel each other at~$y = 0$ will do the same at~$y = b$ only if $2b\sin\theta$ is an integral multiple of~$\lambda$, where $\theta$ is the angle shown in Fig.~\href{I_49.html\#Ch49-F4}{49--4}:
\begin{equation} \label{Eq:I:49:7}
m\lambda = 2b\sin\theta,\quad \text{$m = 0$, $1$, $2$, $\ldots$} \end{equation}

\hypertarget{Ch49-S3-p3}{}
Now in the same way we can make the $y$-axis a nodal line by adding two more functions $-(e^{i\omega t})(e^{+ik_xx + ik_yy})$ and~$+(e^{i\omega t})(e^{+ik_xx - ik_yy})$, each representing a reflection of one of the other two waves from the $x = 0$~line. The condition for a nodal line at~$x = a$ is similar to the one for~$y = b$. It is that $2a\cos\theta$ must also be an integral multiple of~$\lambda$:
\begin{equation} \label{Eq:I:49:8}
n\lambda = 2a\cos\theta.
\end{equation} Then the final result is that the waves bouncing about in the box produce a standing-wave pattern, that is, a definite mode.

\hypertarget{Ch49-S3-p4}{}
So we must satisfy the above two conditions if we are to have a mode. Let us first find the wavelength. This can be obtained by eliminating the angle~$\theta$ from (\href{I_49.html\#mjx-eqn-EqI497}{49.7}) and~(\href{I_49.html\#mjx-eqn-EqI498}{49.8}) to obtain the wavelength in terms of $a$,~$b$, $n$ and~$m$. The easiest way to do that is to divide both sides of the respective equations by $2b$ and~$2a$, square them, and add the two equations together. The result is~$\sin^2\theta +
\cos^2\theta = 1$$\;= (n\lambda/2a)^2 + (m\lambda/2b)^2$, which can be solved for~$\lambda$:
\begin{equation} \label{Eq:I:49:9}
\frac{1}{\lambda^2} =
\frac{n^2}{4a^2} +
\frac{m^2}{4b^2}.
\end{equation} In this way we have determined the wavelength in terms of two integers, and from the wavelength we immediately get the frequency~$\omega$, because, as we know, the frequency is equal to~$2\pi c$ divided by the wavelength.

\hypertarget{Ch49-S3-p5}{}
This result is interesting and important enough that we should deduce it by a purely mathematical analysis instead of by an argument about the reflections. Let us represent the vibration by a superposition of four waves chosen so that the four lines $x = 0$,~$x = a$, $y = 0$, and~$y = b$ are all nodes. In addition we shall require that all waves have the same frequency, so that the resulting motion will represent a mode. From our earlier treatment of light reflection we know that $(e^{i\omega t})(e^{-ik_xx + ik_yy})$ represents a wave travelling in the direction indicated in Fig.~\href{I_49.html\#Ch49-F4}{49--4}. Equation~(\href{I_49.html\#mjx-eqn-EqI496}{49.6}), that is, $k =
\omega/c$, still holds, provided \begin{equation} \label{Eq:I:49:10}
k^2 = k_x^2 + k_y^2. \end{equation} It is clear from the figure that $k_x = k\cos\theta$ and~$k_y = k\sin\theta$.

\hypertarget{Ch49-S3-p6}{}
Now our equation for the displacement, say~$\phi$, of the rectangular drumhead takes on the grand form \begin{equation*} \label{Eq:I:49:11a}
\phi = {[}e^{i\omega t}{]}{[}e^{(-ik_xx + ik_yy)} - e^{(+ik_xx + ik_yy)} - e^{(-ik_xx - ik_yy)} + e^{(+ik_xx - ik_yy)}{]}.
\tag{49.11a} \end{equation*}
Although this looks rather a mess, the sum of these things now is not very hard. The exponentials can be combined to give sine functions, so that the displacement turns out to be \begin{equation*}
\label{Eq:I:49:11b} \phi = {[}4\sin k_xx\sin k_yy{]}{[}e^{i\omega t}{]}.
\tag{49.11b} \end{equation*}

\begin{equation} hidden equation shim to bump the equation number \end{equation}
 In other words, it is a sinusoidal oscillation, all right, with a pattern that is also sinusoidal in both the $x$- and the $y$-direction. Our boundary conditions are of course satisfied at $x = 0$ and~$y = 0$. We also want $\phi$ to be zero when $x = a$ and when $y = b$. Therefore we have to put in two other conditions: $k_xa$ must be an integral multiple of~$\pi$, and $k_yb$ must be another integral multiple of~$\pi$. Since we have seen that $k_x = k\cos\theta$ and~$k_y = k\sin\theta$, we immediately get equations (\href{I_49.html\#mjx-eqn-EqI497}{49.7}) and~(\href{I_49.html\#mjx-eqn-EqI498}{49.8}) and from these the final result~(\href{I_49.html\#mjx-eqn-EqI499}{49.9}).

\hypertarget{Ch49-S3-p7}{}
Now let us take as an example a rectangle whose width is twice the height. If we take~$a = 2b$ and use Eqs. (\href{I_49.html\#mjx-eqn-EqI494}{49.4}) and~(\href{I_49.html\#mjx-eqn-EqI499}{49.9}), we can calculate the frequencies of all of the modes: \begin{equation}
\label{Eq:I:49:12} \omega^2 =
\biggl(\frac{\pi c}{b}\biggr)^2 \frac{4m^2 + n^2}{4}. \end{equation}
Table~\href{I_49.html\#Ch49-T1}{49--1} lists a few of the simple modes and also shows their shape in a qualitative way.

\hypertarget{Ch49-T1}{}
{Table 49--1}

\begin{longtable}[]{@{}lllll@{}}
\toprule Mode & $m$ & $n$ & $(\omega/\omega_0)^2$ & $\omega/\omega_0$\tabularnewline {} \includegraphics[width=0.75\textwidth]{img/plus_mode.pdf} & $1$ & $1$ & $1.25$ & $1.12$\tabularnewline \includegraphics[width=0.75\textwidth]{img/pm_mode.pdf} & $1$ & $2$ & $2.00$ & $1.41$\tabularnewline \includegraphics[width=0.75\textwidth]{img/pmp_mode.pdf} & $1$ & $3$ & $3.25$ & $1.80$\tabularnewline \includegraphics[width=0.75\textwidth]{img/vert_pm_mode.pdf} & $2$ & $1$ & $4.25$ & $2.06$\tabularnewline \includegraphics[width=0.75\textwidth]{img/pmpm_mode.pdf} & $2$ & $2$ & $5.00$ & $2.24$\tabularnewline \bottomrule \end{longtable}

\hypertarget{Ch49-S3-p8}{}
The most important point to be emphasized about this particular case is that the frequencies are not multiples of each other, nor are they multiples of any number. The idea that the natural frequencies are harmonically related is not generally true. It is not true for a system of more than one dimension, nor is it true for one-dimensional systems which are more complicated than a string with uniform density and tension. A simple example of the latter is a hanging chain in which the tension is higher at the top than at the bottom. If such a chain is set in harmonic oscillation, there are various modes and frequencies, but the frequencies are not simple multiples of any number, nor are the mode shapes sinusoidal.

\hypertarget{Ch49-S3-p9}{}
The modes of more complicated systems are still more elaborate. For example, inside the mouth we have a cavity above the vocal cords, and by moving the tongue and the lips, and so forth, we make an open-ended pipe or a closed-ended pipe of different diameters and shapes; it is a terribly complicated resonator, but it is a resonator nevertheless. Now when one talks with the vocal cords, they are made to produce some kind of tone. The tone is rather complicated and there are many sounds coming out, but the cavity of the mouth further modifies that tone because of the various resonant frequencies of the cavity. For instance, a singer can sing various vowels, a, or o, or oo, and so forth, at the same pitch, but they sound different because the various harmonics are in resonance in this cavity to different degrees. The very great importance of the resonant frequencies of a cavity in modifying the voice sounds can be demonstrated by a simple experiment. Since the speed of sound goes as the reciprocal of the square root of the density, the speed of sound may be varied by using different gases. If one uses helium instead of air, so that the density is lower, the speed of sound is much higher, and all the frequencies of a cavity will be raised. Consequently if one fills one's lungs with helium before speaking, the character of his voice will be drastically altered even though the vocal cords may still be vibrating at the same frequency.

\hypertarget{Ch49-S4}{}
\subsubsection{\texorpdfstring{{49--4}Coupled pendulums}{49--4Coupled pendulums}}\label{coupled-pendulums}

\hypertarget{Ch49-S4-p1}{}
Finally we should emphasize that not only do modes exist for complicated continuous systems, but also for very simple mechanical systems. A good example is the system of two coupled pendulums discussed in the preceding chapter. In that chapter it was shown that the motion could be analyzed as a superposition of two harmonic motions with different frequencies. So even this system can be analyzed in terms of harmonic motions or modes. The string has an infinite number of modes and the two-dimensional surface also has an infinite number of modes. In a sense it is a double infinity, if we know how to count infinities. But a simple mechanical thing which has only two degrees of freedom, and requires only two variables to describe it, has only two modes.

\hypertarget{Ch49-F5}{}
\includegraphics[width=0.75\textwidth]{/Users/jcprime/flp/img/FLP_I/f49-05/f49-05_tc_big.pdf}
 {Fig. 49--5.}Two coupled pendulums.

\hypertarget{Ch49-S4-p2}{}
Let us make a mathematical analysis of these two modes for the case where the pendulums are of equal length. Let the displacement of one be~$x$, and the displacement of the other be~$y$, as shown in Fig.~\href{I_49.html\#Ch49-F5}{49--5}. Without a spring, the force on the first mass is proportional to the displacement of that mass, because of gravity. There would be, if there were no spring, a certain natural frequency~$\omega_0$ for this one alone. The equation of motion without a spring would be \begin{equation}
\label{Eq:I:49:13}
m\,\frac{d^2x}{dt^2} = -m\omega_0^2x. \end{equation} The other pendulum would swing in the same way if there were no spring. In addition to the force of restoration due to gravitation, there is an additional force pulling the first mass. That force depends upon the excess distance of $x$ over~$y$ and is proportional to that difference, so it is some constant which depends on the geometry, times~$(x - y)$. The same force in reverse sense acts on the second mass. The equations of motion that have to be solved are therefore \begin{equation} \label{Eq:I:49:14}
m\,\frac{d^2x}{dt^2} = -m\omega_0^2x - k(x - y),\quad m\,\frac{d^2y}{dt^2} = -m\omega_0^2y - k(y - x).
\end{equation}

\hypertarget{Ch49-S4-p3}{}
In order to find a motion in which both of the masses move at the same frequency, we must determine how much each mass moves. In other words, pendulum~$x$ and pendulum~$y$ will oscillate at the same frequency, but their amplitudes must have certain values, $A$ and~$B$, whose relation is fixed. Let us try this solution:
\begin{equation} \label{Eq:I:49:15} x = Ae^{i\omega t},\quad y = Be^{i\omega t}. \end{equation} If these are substituted in Eqs.~(\href{I_49.html\#mjx-eqn-EqI4914}{49.14}) and similar terms are collected, the results are \begin{equation} \begin{aligned}
\biggl(\omega^2 -
\omega_0^2 -
\frac{k}{m}\biggr)A & = -\frac{k}{m}\,B, \biggl(\omega^2 -
\omega_0^2 -
\frac{k}{m}\biggr)B & = -\frac{k}{m}\,A.
\end{aligned} \label{Eq:I:49:16}
\end{equation} The equations as written have had the common factor~$e^{i\omega t}$ removed and have been divided by~$m$.

\hypertarget{Ch49-S4-p4}{}
Now we see that we have two equations for what looks like two unknowns. But there really are not \emph{two} unknowns, because the whole size of the motion is something that we cannot determine from these equations. The above equations can determine only the \emph{ratio} of $A$ to~$B$, \emph{but they must both give the same ratio}. The necessity for both of these equations to be consistent is a requirement that the frequency be something very special.

\hypertarget{Ch49-S4-p5}{}
In this particular case this can be worked out rather easily. If the two equations are multiplied together, the result is \begin{equation} \label{Eq:I:49:17}
\biggl(\omega^2 -
\omega_0^2 -
\frac{k}{m}\biggr)^2AB =
\biggl(\frac{k}{m}\biggr)^2AB.
\end{equation} The term~$AB$ can be removed from both sides unless $A$ and~$B$ are zero, which means there is no motion at all. If there is motion, then the other terms must be equal, giving a quadratic equation to solve. The result is that there are two possible frequencies: \begin{equation}
\label{Eq:I:49:18} \omega_1^2 =
\omega_0^2,\quad \omega_2^2 = \omega_0^2 +
\frac{2k}{m}. \end{equation}
Furthermore, if these values of frequency are substituted back into Eq.~(\href{I_49.html\#mjx-eqn-EqI4916}{49.16}), we find that for the first frequency~$A = B$, and for the second frequency~$A = -B$. These are the ``mode shapes'', as can be readily verified by experiment.

\hypertarget{Ch49-S4-p6}{}
It is clear that in the first mode, where $A = B$, the spring is never stretched, and both masses oscillate at the frequency~$\omega_0$, as though the spring were absent. In the other solution, where $A = -B$, the spring contributes a restoring force and raises the frequency. A more interesting case results if the pendulums have different lengths. The analysis is very similar to that given above, and is left as an exercise for the reader.

\hypertarget{Ch49-S5}{}
\subsubsection{\texorpdfstring{{49--5}Linear systems}{49--5Linear systems}}\label{linear-systems}

\hypertarget{Ch49-S5-p1}{}
Now let us summarize the ideas discussed above, which are all aspects of what is probably the most general and wonderful principle of mathematical physics. If we have a linear system whose character is independent of the time, then the motion does not have to have any particular simplicity, and in fact may be exceedingly complex, but there are very special motions, usually a series of special motions, in which the whole pattern of motion varies exponentially with the time. For the vibrating systems that we are talking about now, the exponential is imaginary, and instead of saying ``exponentially'' we might prefer to say ``sinusoidally'' with time. However, one can be more general and say that the motions will vary exponentially with the time in very special modes, with very special shapes. The most general motion of the system can always be represented as a superposition of motions involving each of the different exponentials.

\hypertarget{Ch49-S5-p2}{}
This is worth stating again for the case of sinusoidal motion: a linear system need not be moving in a purely sinusoidal motion, i.e.~at a definite single frequency, but no matter how it does move, this motion can be represented as a superposition of pure sinusoidal motions. The frequency of each of these motions is a characteristic of the system, and the pattern or waveform of each motion is also a characteristic of the system. The general motion in any such system can be characterized by giving the strength and the phase of each of these modes, and adding them all together. Another way of saying this is that any linear vibrating system is equivalent to a set of independent harmonic oscillators, with the natural frequencies corresponding to the modes.

\hypertarget{Ch49-S5-p3}{}
We conclude this chapter by remarking on the connection of modes with quantum mechanics. In quantum mechanics the vibrating object, or the thing that varies in space, is the amplitude of a probability function that gives the probability of finding an electron, or system of electrons, in a given configuration. This amplitude function can vary in space and time, and satisfies, in fact, a linear equation. But in quantum mechanics there is a transformation, in that what we call frequency of the probability amplitude is equal, in the classical idea, to energy. Therefore we can translate the principle stated above to this case by taking the word \emph{frequency} and replacing it with \emph{energy}. It becomes something like this: a quantum-mechanical system, for example an atom, need not have a definite energy, just as a simple mechanical system does not have to have a definite frequency; but no matter how the system behaves, its behavior can always be represented as a superposition of states of definite energy. The energy of each state is a characteristic of the atom, and so is the pattern of amplitude which determines the probability of finding particles in different places. The general motion can be described by giving the amplitude of each of these different energy states. This is the origin of energy levels in quantum mechanics. Since quantum mechanics is represented by waves, in the circumstance in which the electron does not have enough energy to ultimately escape from the proton, they are \emph{confined waves}. Like the confined waves of a string, there are definite frequencies for the solution of the wave equation for quantum mechanics. The quantum-mechanical interpretation is that these are definite \emph{energies}. Therefore a quantum-mechanical system, because it is represented by waves, can have definite states of fixed energy; examples are the energy levels of various atoms.
